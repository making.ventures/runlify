import { Doc } from './doc'
import { TemplateDoc } from './templateDoc'

export const templateDocToDoc = (template: TemplateDoc): Doc => {
  return template
}

export default templateDocToDoc
