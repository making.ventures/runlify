import SystemMetaBuilder from '../builders/SystemMetaBuilder';

const addCommonCommands = (system: SystemMetaBuilder) => {
  system.addCommnad(
    'back',
    'build',
    '(rm -rf dist || true) && tsc',
    'Сборки проекта',
  );
  system.addCommnad(
    'back',
    'start',
    'runlify start env=prod node --unhandled-rejections=strict dist/index.js',
    'Запуска собранного проекта',
  );
  system.addCommnad(
    'back',
    'dev',
    'ts-node-dev --files src/index.ts',
    'Запуска преокта в режиме разработки',
  );
  system.addCommnad(
    'back',
    'dev:stage',
    'runlify start env=stage yarn dev',
    'Запуска преокта в режиме разработки с использованием stage окружения',
  );
  system.addCommnad(
    'back',
    'dev:local',
    'runlify start env=local yarn dev',
    'Запуска преокта в режиме разработки с использованием локального окружения',
  );
  system.addCommnad(
    'back',
    'dev:prod',
    'runlify start env=prod yarn dev',
    'Запуска преокта в режиме разработки с использованием prod окружения',
  );
  system.addCommnad(
    'back',
    'lint',
    'eslint ./src/**.ts',
    'Проверки проекта линтером',
  );
  system.addCommnad(
    'back',
    'gen',
    'graphql-codegen --config codegen.yml',
    'Генерации typescript-типов на основе graphql-типов',
  );
  system.addCommnad(
    'back',
    'test',
    'jest --maxWorkers 2',
    'Запуска тестов',
  );
  system.addCommnad(
    'back',
    'prisma:gen',
    'prisma generate',
    'Генерации prisma-клиента',
  );
  system.addCommnad(
    'back',
    'prisma:newMigration',
    'runlify start env=migration prisma migrate dev --preview-feature',
    'Создания новой миграции базы данных',
  );
  system.addCommnad(
    'back',
    'prisma:deploy',
    'prisma migrate deploy --schema prisma/deployConnection.prisma',
    'Мигрирования базы данных',
  );
  system.addCommnad(
    'back',
    'prisma',
    'prisma',
    'Запуска cli призмы из зависимостей проекта',
  );
  system.addCommnad(
    'back',
    'ts-node:withContext',
    'yarn ts-node ./src/init/wrap.ts',
    'Запуска typescript скрипта, требующего для работы контекст',
  );
  system.addCommnad(
    'back',
    'init:base',
    'yarn ts-node src/init/baseInit.ts',
    'Инициализации базы данных',
  );
  system.addCommnad(
    'back',
    'init:permissions',
    'yarn ts-node:withContext src/init/roles/initRoles.ts',
    'Инициализации системы ролевой модели',
  );
  system.addCommnad(
    'back',
    'init:dev',
    'yarn ts-node src/init/initDev.ts',
    'Инициализации базы данных для разработчика или тестировщика',
  );
};

export default addCommonCommands;
