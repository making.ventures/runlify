import {
  BootstrapEntityOptions,
  EntityBuilderWithOptions,
  defaultBootstrapEntityOptions,
} from '../types'
import CatalogBuilder from './CatalogBuilder'
import DocumentBuilder from './DocumentBuilder'
import { InfoRegistryBuilder } from './InfoRegistryBuilder'
import SumRegistryBuilder, { RegistryOptions } from './SumRegistryBuilder'
import {
  Command,
  ConfigValue,
  ConfigVar,
  ConfigVarScope,
  DeployEnvironment,
  FieldType,
  Glossary,
  Language,
  MethodType,
  ProjectCategory,
  System,
} from './buildedTypes'
import { addFilesCatalog } from '../defaultCatalogs'
import * as R from 'ramda'
import ReportBuilder from './ReportBuilder'
import RestApiBuilder from './RestApiBuilder'
import DeploymentBuilder from './DeploymentBuilder'
import RoleBuilder from './RoleBuilder'
import TelegramBotBuilder from './TelegramBotBuilder'
import {ConfigVarBuilder} from './ConfigVarBuilder';
import log from '../../log';
import IntegrationClientBuilder from './integrationClients/IntegrationClientBuilder'
import ExternalMenuItemBuilder from './menu/ExternalMenuItemBuilder'
import ExternalEnvMenuItemBuilder from './menu/ExternalEnvMenuItemBuilder'
import InternalMenuItemBuilder from './menu/InternalMenuItemBuilder'
import GroupMenuItemBuilder from './menu/GroupMenuItemBuilder'
import PageBuilder from './PageBuilder'
import AdditionalServiceBuilder from './AdditionalServiceBuilder'
import BaseModelBuilder from './mehods/BaseModelBuilder'
import MethodBuilder, { MethodsModelsHolder } from './mehods/MethodBuilder'

export const defaultConfigVar: Omit<ConfigVar, 'name' | 'type'> = {
  needFor: '',
  default: '',
  required: true,
  scopes: ['back', 'worker', 'telegramBot'],
  hidden: false,
  editable: true,
}

class SystemMetaBuilder implements MethodsModelsHolder {
  catalogs: EntityBuilderWithOptions<CatalogBuilder>[] = []
  reports: EntityBuilderWithOptions<ReportBuilder>[] = []
  documents: EntityBuilderWithOptions<DocumentBuilder>[] = []
  infoRegistries: EntityBuilderWithOptions<InfoRegistryBuilder>[] = []
  sumRegistries: EntityBuilderWithOptions<SumRegistryBuilder>[] = []
  configVars: ConfigVarBuilder<any>[] = []
  glossary: Glossary[] = []
  commands: Command[] = []
  deployEnvironments: DeployEnvironment[] = []
  telegramBots: TelegramBotBuilder[] = []
  languages: Language[] = []
  restApis: RestApiBuilder[] = []
  integrationClients: IntegrationClientBuilder[] = []
  workers: DeploymentBuilder[] = []
  roles: RoleBuilder[] = []
  defaultLanguage: string
  defOpts: BootstrapEntityOptions
  menuItems: (GroupMenuItemBuilder | InternalMenuItemBuilder | ExternalMenuItemBuilder | ExternalEnvMenuItemBuilder)[] = [];
  pages: PageBuilder[] = [];
  protected methods: MethodBuilder[] = []
  protected models: BaseModelBuilder[] = []
  labels: string[] = [];
  additionalServices: AdditionalServiceBuilder[] = [];

  name: string
  prefix: string
  needFor?: string

  back: DeploymentBuilder

  // requests: MemoryAndCpu = { memory: '128Mi', cpu: '0.15' }
  // limits: MemoryAndCpu = { memory: '256Mi', cpu: '1' }

  // space = '';
  constructor(
    prefix: string,
    defOpts: BootstrapEntityOptions = defaultBootstrapEntityOptions,
    defaultLanguage = 'ru'
  ) {
    this.defOpts = defOpts
    this.prefix = prefix
    this.setPrefix(prefix)
    this.name = prefix
    this.setName(prefix)
    this.back = new DeploymentBuilder('back', defaultLanguage);

    this.addConfigVar('dockerRegistry.domain', 'string', false, '', 'Docker registry domain', ['ci']);
    this.addConfigVar('dockerRegistry.username', 'string', false, '', 'Docker registry username', ['ci']);
    this.addConfigVar('dockerRegistry.password', 'string', false, '', 'Docker registry password', ['ci']);

    this.addConfigVar(
      'app.name',
      'string',
      true,
      this.defOpts.projectPrefix,
      'Техническое название приложения'
    )
    this.addConfigVar(
      'app.title',
      'string',
      false,
      this.defOpts.projectName,
      'Пользовательское название приложения'
    )
    this.addConfigVar(
      'app.environment',
      'string',
      true,
      'dev',
      'Название окружения'
    )
    this.addConfigVar(
      'app.ui.url',
      'string',
      false,
      'http://localhost:4000',
      'Урл интерфейса конечного пользователя (без конечного слеша)'
    )

    // Main database
    this.addConfigVar(
      'database.main.write.uri',
      'string',
      true,
      'postgresql://postgres:password@localhost:5432',
      'Строка подключения к основной базе данных для записи'
    ).setSecure()
    this.addConfigVar(
      'database.main.readOnly.uri',
      'string',
      false,
      'postgresql://postgres:password@localhost:5432',
      'Строка подключения к основной базе только для чтения'
    ).setSecure()
    this.addConfigVar(
      'database.main.readOnly.enabled',
      'bool',
      true,
      false,
      'Включена ли работа с запросами на чтение через отдельное подключение'
    )
    this.addConfigVar(
      'database.main.migration.uri',
      'string',
      true,
      'postgresql://postgres:password@localhost:5432',
      'Строка подключения к основной базе для миграций (должна быть прямой строкой подключения к бд минуя pgbouncer, если он используется)'
    ).setSecure()

    this.addConfigVar(
      'adm.jwt.secret',
      'string',
      false,
      'admSecret',
      'Секрет для подписи JWT-токенов приложения админки'
    ).setSecure()
    this.addConfigVar(
      'app.jwt.secret',
      'string',
      false,
      'appSecret',
      'Секрет для подписи JWT-токенов приложения пользователей'
    ).setSecure()

    this.addConfigVar(
      's3.endpoint',
      'string',
      true,
      's3.eu-central-1.wasabisys.com',
      'Эндпоинт S3, который использует бекенд',
    ).setSecure()
    this.addConfigVar(
      's3.public.endpoint',
      'string',
      false,
      undefined,
      'Эндпоинт S3, доступный для фронтенда',
    ).setSecure()
    // this.addConfigVar(
    //   's3.publicEndpoint',
    //   'string',
    //   true,
    //   's3.eu-central-1.wasabisys.com',
    //   'Публичныйм эндпоинт S3, к которому имеет доступ полььзователь',
    // )
    this.addConfigVar(
      's3.region',
      'string',
      true,
      'eu-central-1',
      'Идентификатор доступа для авторизации в S3'
    )
    this.addConfigVar(
      's3.accessKeyId',
      'string',
      true,
      '',
      'Идентификатор доступа для авторизации в S3'
    )
    this.addConfigVar(
      's3.secretAccessKey',
      'string',
      true,
      '',
      'Секретный ключ для авторизации в S3'
    ).setSecure()
    this.addConfigVar(
      's3.bucket.emailFiles',
      'string',
      true,
      '',
      'Имя бакета для хранения файлов, отправленных на почту',
    );
    this.addConfigVar(
      's3.bucket.tmpFilesToDownload',
      'string',
      true,
      '',
      'Имя бакета для хранения временных файлов, созданных для скачивания пользователями',
    );

    this.addConfigVar(
      'admin.ui.url',
      'string',
      false,
      'http://localhost:8080',
      'Урл административного интерфейса (без конечного слеша)'
    )
    this.addConfigVar(
      'admin.recaptcha.secretKey',
      'string',
      false,
      '',
      'Секретный токен рекапчи приложения админки'
    ).setSecure()
    this.addConfigVar(
      'admin.recaptcha.requiredScore',
      'float',
      false,
      0.7,
      'Требуемый уровень доверия к пользователю'
    )
    this.addConfigVar(
      'endpoint',
      'string',
      false,
      '',
      'Путь к бекенду',
      ['admin-app']
    )
    this.addConfigVar(
      'admin.recaptcha.publicKey',
      'string',
      false,
      '',
      'Публичный токен рекапчи приложения админки',
      ['admin-app']
    )

    this.addConfigVar(
      'customer.recaptcha.secretKey',
      'string',
      false,
      '',
      'Секретный токен рекапчи приложения пользователя'
    ).setSecure()
    this.addConfigVar(
      'customer.recaptcha.requiredScore',
      'string',
      false,
      '0.7',
      'Требуемый уровень доверия к пользователю'
    )
    this.addConfigVar(
      'customer.recaptcha.publicKey',
      'string',
      false,
      '',
      'Публичный токен рекапчи приложения пользователя',
      ['cutomer-app']
    )

    this.addConfigVar(
      'loki.url',
      'string',
      false,
      '',
      'Урл для доступа в Loki. Используется для запроса бизнес-логов'
    )

    this.addConfigVar(
      'logs.format',
      'string',
      false,
      'plain',
      'Формат логов (plain | json)'
    )

    this.addConfigVar(
      'graphql.playground.enabled',
      'bool',
      false,
      true,
      'Включение graphql playground (true | false)'
    )

    this.addConfigVar('bootstrap.enabled', 'bool', false, false, 'Подготавливать окружеине при запуске');
    this.addConfigVar('authorization.backendChecks.enabled', 'bool', false, false, 'Проверка авторизации на бекенде включены');

    // kafka
    this.addConfigVar('kafka.enabled', 'bool', false, false, 'Кафка включена');
    this.addConfigVar('kafka.brokers', 'string', false, 'localhost:29092;localhost:29094', 'Список kafka блокеров');
    this.addConfigVar('kafka.username', 'string', false, '', 'Username доступа в kafka');
    this.addConfigVar('kafka.password', 'string', false, '', 'Пароль доступа в kafka').setSecure();
    this.addConfigVar('kafka.queue.maxAttemptsSize', 'int', false, 10, 'Максимальное количество попыток обработки ошибки на сообщение');
    this.addConfigVar('kafka.queue.defaultRetryTime', 'int', false, 20000, 'Время паузы после первой ошибки, например 20000 мс, потом оно увеличивается экспоненциально с мультипликатором 1.5');
    this.addConfigVar('kafka.queue.waitingInterruptTime', 'int', false, 60000, 'Время паузы в очереди ожидания, когда она прошла все сообщения, это чтобы она не крутила сообщения покругу без остановки ');
    this.addConfigVar('kafka.queue.stackSize', 'int', false, 3, 'Количество сообщений, обрабатываемых параллельно');
    this.addConfigVar('kafka.queue.supportedVersion', 'string', false, '1;2', 'Поддерживаемые версии сообщения');
    // this.addConfigVar('kafka.queue.autoCommitInterval', 'int', false, 10000, 'Потребитель будет фиксировать смещения по истечении заданного периода, например, пяти секунд. Значение в миллисекундах  ');
    // this.addConfigVar('kafka.queue.autoCommitThreshold', 'int', false, 1000, 'Потребитель будет фиксировать смещения после разрешения заданного количества сообщений, например тысячи сообщений');
    this.addConfigVar('kafka.queue.acks', 'int', false, 1, '`-1`(all) все несинхронизированные реплики должны подтвердить (по умолчанию), `0` нет подтверждений, `1` только ждет подтверждения лидера');
    this.addConfigVar('kafka.auth.enabled', 'bool', true, true, 'Включена проверка аутентификации');
    this.addConfigVar('kafka.ssl.enabled', 'bool', true, true, 'Включена работа по ssl');
    this.addConfigVar('kafka.ssl.rejectUnauthorized', 'bool', false, false, 'Запрещать невалидный ssl сертификат');

    // es
    this.addConfigVar('es.enabled', 'bool', false, false, 'Эластик включен');
    this.addConfigVar('es.cloudId', 'string', false, '', 'Идентификатор аккаунта в облачном сервисе ElasticSearch');
    this.addConfigVar('es.username', 'string', false, '', 'Пользователь для авторизации в облачном сервисе ElasticSearch');
    this.addConfigVar('es.password', 'string', false, '', 'Пароль для авторизации в облачном сервисе ElasticSearch').setSecure();
    this.addConfigVar('es.node', 'string', false, 'http://localhost:9200', 'Нода эластика');
    this.addConfigVar('es.tls.rejectUnauthorized', 'bool', false, false, 'Запрещать невалидный ssl сертификат');

    this.addConfigVar('sentry.dsn', 'string', false, '', 'Sentry dsn');

    // oidc
    this.addConfigVar('oidc.adm.jwksRejectUnauthorized', 'bool', false, false, 'Запрещать невалидный ssl сертификат');
    this.addConfigVar('oidc.adm.url', 'string', false, undefined, 'Хост oidc для админки', ['back', 'admin-app']);
    this.addConfigVar('oidc.adm.realm', 'string', false, undefined, 'Реалм oidc для админки', ['back', 'admin-app']);
    this.addConfigVar('oidc.adm.jwksUri', 'string', false, undefined, 'Uri jwks oidc для проверки авторизации админки', ['back']);
    this.addConfigVar('oidc.adm.issuer', 'string', false, undefined, 'Issuer oidc для админки', ['back', 'admin-app']);
    this.addConfigVar('oidc.adm.logoutType', 'string', false, undefined, 'Тип выхода админки', ['admin-app']);
    this.addConfigVar('oidc.adm.clientId', 'string', false, undefined, 'Идентификатор клиента oidc для админки', ['admin-app']);
    this.addConfigVar('oidc.app.url', 'string', false, undefined, 'Хост oidc для приложения пользователя', ['back', 'cutomer-app']);
    this.addConfigVar('oidc.app.realm', 'string', false, undefined, 'Реалм oidc для приложения пользователя', ['back', 'cutomer-app']);
    this.addConfigVar('oidc.app.clientId', 'string', false, undefined, 'Идентификатор клиента oidc для приложения пользователя', ['cutomer-app']);

    this.addConfigVar('keycloak.adm.cli.client', 'string', false, 'backend-api', 'Клиент для доступа в api keycloak админки');
    this.addConfigVar('keycloak.adm.cli.secret', 'string', false, undefined, 'Секрет для доступа в api keycloak админки');
    this.addConfigVar('keycloak.app.cli.client', 'string', false, 'backend-api', 'Клиент для доступа в api keycloak приложения пользователя');
    this.addConfigVar('keycloak.app.cli.secret', 'string', false, undefined, 'Секрет для доступа в api keycloak приложения пользователя');

    // login check
    this.addConfigVar('checkLoginIframe', 'bool', false, false, 'Проверка авторизации во фрейме включена', ['back', 'admin-app']);

    // 'admin-app'

    this.addDeployEnvironment({
      name: 'dev',
      manualDeploy: false,
      main: false,
      clusterName: 'stage01',
      workerClusterName: 'workers01',
      branchName: 'master',
      metricsEnabled: true,
      host: 'making.ventures',
      runnerTag: 'dev',
      gitlabEnvPrefix: 'dev',
    })
    // this.addDeployEnvironment({
    //   name: 'stage',
    //   manualDeploy: false,
    //   clusterName: 'stage01',
    //   branchName: 'stage',
    // })
    this.addDeployEnvironment({
      name: 'prod',
      manualDeploy: true,
      main: true,
      clusterName: 'stage01',
      workerClusterName: 'workers01',
      branchName: 'release',
      metricsEnabled: true,
      host: 'making.ventures',
      runnerTag: 'prod',
      gitlabEnvPrefix: 'prod',
    })
    this.addLanguage('en', 'English')
    this.addLanguage('ru', 'Russian')
    if (!['en', 'ru'].includes(defaultLanguage)) {
      this.addLanguage(defaultLanguage)
    }

    this.defaultLanguage = defaultLanguage

    // this.addTelegramBot('hello');

    this.initDefaultCatalogs()

    // this
    //   .addWorker('general', 'Таски общего назначения')
    //   .setNeedFor('Для выполнения тасков общего назначения');

    // this
    //   .addWorker('emails', 'Отправка почты')
    //   .setNeedFor('Для отправки почты');
  }

  setName(name: string) {
    this.name = name

    return this
  }

  setNeedFor(needFor: string) {
    this.needFor = needFor

    return this
  }

  setPrefix(prefix: string) {
    this.prefix = prefix

    return this
  }


  // | 'string'
  // | 'int'
  // | 'bigint'
  // | 'float'
  // | 'bool'
  // | 'datetime'
  // | 'date'

  addConfigVar<T extends FieldType>(
    name: string,
    type: T,
    required: boolean,
    def: ConfigValue<T> | undefined,
    needFor: string,
    scopes: ConfigVarScope[] = ['back', 'worker', 'telegramBot'],
    hidden = false,
    editable = true,
  ): ConfigVarBuilder<T> {
    if (this.configVars.some((v) => v.name === name)) {
      throw new Error(`"${name}" config var already exists`)
    }

    const configVar = new ConfigVarBuilder(name, type, required, def, needFor, scopes, hidden, editable)

    this.configVars.push(configVar)

    return configVar
  }

  delConfigVar(
    name: string,
  ) {
    if (!this.configVars.some((v) => v.name === name)) {
      throw new Error(`There is no "${name}" config var`)
    }

    this.configVars = this.configVars.filter(v => v.name !== name)

    return this
  }

  getConfigVar(name: string): ConfigVarBuilder<any> | undefined {
    return this.configVars.find(el => el.name === name)
  }

  getConfigVarRequired(name: string): ConfigVarBuilder<any> {
    const configVars = this.configVars.find(el => el.name === name)

    if (!configVars) {
      throw new Error(`There is no "${name}" config var`)
    }

    return configVars
  }

  setConfigVarDefaultValue<T extends FieldType>(
    name: string,
    def: ConfigValue<T> | undefined,
  ) {
    log.warn('setConfigVarDefaultValue is legacy, legacy, use system.getConfigVarRequired(name).setDefValue(def) instead')
    const configVars = this.getConfigVarRequired(name);

    configVars.setDefValue(def);

    return this
  }

  setDefaultValueForConfigVar(name: string, def: string) {
    log.warn('setDefaultValueForConfigVar is legacy, legacy, use system.getConfigVarRequired(name).setDefValue(def) instead')
    return this.setConfigVarDefaultValue(name, def);
  }

  addLanguage(id: string, title?: string) {
    if (!this.languages.map(({id}) => id).includes(id)) {
      this.languages.push({id, title: title ?? id})
    }
  }

  deleteLanguage(id: string) {
    this.languages = this.languages.filter(lang => lang.id !== id)
  }

  setDefailtLanguage(id: string) {
    if (!this.languages.map(({id}) => id).includes(id)) {
      throw new Error(`There is no "${id}" langiage`)
    }

    this.defaultLanguage = id
  }

  addDeployEnvironment(deployEnvironment: DeployEnvironment) {
    if (this.deployEnvironments.some((f) => f.name === deployEnvironment.name)) {
      throw new Error(`There is already deployEnvironment with name "${deployEnvironment.name}"`)
    }

    this.deployEnvironments.push(deployEnvironment)

    // if (this.deployEnvironments.filter((f) => f.main)?.length > 1) {
    //   throw new Error(`Main deployEnvironment shuold be only one"`)
    // }

    return this
  }

  editDeployEnvironment(name: string, deployEnvironment: Partial<DeployEnvironment>) {
    if (!this.deployEnvironments.some((f) => f.name === name)) {
      throw new Error(`There is no "${name}" deploy environment (you may use ${this.deployEnvironments.map(e => e.name).join(', ')})`)
    }

    this.deployEnvironments = this.deployEnvironments.map(env => env.name === name ? {
      ...env,
      ...deployEnvironment,
      name,
    } : env)

    // if (this.deployEnvironments.filter((f) => f.main)?.length > 1) {
    //   throw new Error(`Main deployEnvironment shuold be only one"`)
    // }

    return this
  }

  delDeployEnvironment(name: string) {
    if (!this.deployEnvironments.some((f) => f.name === name)) {
      throw new Error(`There is no "${name}" deploy environment (you may use ${this.deployEnvironments.map(e => e.name).join(', ')})`)
    }

    this.deployEnvironments = this.deployEnvironments.filter(v => v.name !== name)

    return this
  }

  addTelegramBot(name: string, title?: string) {
    if (this.telegramBots.some((f) => f.name === name)) {
      throw new Error(`There is already telegramBot with name "${name}"`)
    }

    const telegramBot = new TelegramBotBuilder(
      name,
      this.defaultLanguage,
      title
    )

    this.telegramBots.push(telegramBot)

    return telegramBot
  }

  // catalogs
  getCatalogs(): EntityBuilderWithOptions[] {
    return this.catalogs
  }

  addCatalog(
    name: string,
    title?: {
      singular?: string,
      plural?: string,
    },
    options = this ? this.defOpts : ({} as any)
  ) {
    this.assureNoNamedEntityAlreadyDefined(name)

    const catalog = new CatalogBuilder(name, this.defaultLanguage, title)

    this.catalogs.push({ entity: catalog, options })

    return catalog
  }

  getCatalogByName(name: string) {
    const catalog = this.catalogs.find((c) => c.entity.name === name)

    if (!catalog) {
      throw new Error(`There is no "${name}" catalog`)
    }

    return catalog.entity
  }

  getInfoRegistryByName(name: string) {
    const infoRegistry = this.infoRegistries.find((c) => c.entity.name === name)

    if (!infoRegistry) {
      throw new Error(`There is no "${name}" infoRegistry`)
    }

    return infoRegistry.entity
  }

  addRestApi(name: string, path: string, title?: string, auth = true) {
    if (this.restApis.some((f) => f.name === name)) {
      throw new Error(`There is already rest api with name "${name}"`)
    }

    if (this.restApis.some((f) => f.path === path)) {
      throw new Error(`There is already rest api with path "${path}"`)
    }

    const restApi = new RestApiBuilder(name, path, this.defaultLanguage, title, auth)

    this.restApis.push(restApi)

    return restApi
  }

  addIntegrationClient(
    name: string,
    title?: string,
  ) {
    if (
      this.integrationClients.some((f) => f.name === name)
    ) {
      throw new Error(
        `There is already integration client with name "${name}". Entity ${this.name}`
      )
    }

    const integrationClient = new IntegrationClientBuilder(name, this.defaultLanguage, title)

    this.integrationClients.push(integrationClient)

    return integrationClient;
  }

  addAdditionalService(
    name: string,
    title?: string,
  ) {
    if (
      this.additionalServices.some((f) => f.name === name)
    ) {
      throw new Error(
        `There is already additional service with name "${name}". Entity ${this.name}`
      )
    }

    const additionalService = new AdditionalServiceBuilder(name, this.defaultLanguage, title)

    this.additionalServices.push(additionalService)

    return additionalService;
  }

  addWorker(name: string, title?: string) {
    if (this.workers.some((f) => f.name === name)) {
      throw new Error(`There is already rest api with name "${name}"`)
    }

    const worker = new DeploymentBuilder(name, this.defaultLanguage, title)

    this.workers.push(worker)

    return worker
  }

  addManyToManyRelation(
    name: string,
    title?: {
      singular?: string,
      plural?: string,
    },
    options = this ? this.defOpts : ({} as any)
  ) {
    this.assureNoNamedEntityAlreadyDefined(name)

    const catalog = new CatalogBuilder(name, this.defaultLanguage, title)
    this.catalogs.push({ entity: catalog, options })

    return catalog
  }

  addRole(
    name: string,
    title?: string,
  ) {
    this.assureNoNamedEntityAlreadyDefined(name)

    const role = new RoleBuilder(name, this.defaultLanguage, title)

    this.roles.push(role)

    return role
  }
  
  // constructor(assurePageExists: (pageId: string) => void, name: string, defaultLanguage: string, title?: string, level?: number) {
  //   super(name, defaultLanguage, title, level);

  //   this.assurePageExists = assurePageExists;
  // }

  getAllPages() {
    return [
      ...this.pages,
      ...this.reports.map(e => e.entity.getPage()),
      ...this.getSavableEntities().map(e => e.entity.pages),
    ].flat();
  }

  assurePageExists(pageName: string) {
    if (!this.getAllPages().some(p => p.name === pageName)) {
      throw new Error(`There is no "${pageName}" page`);
    }
  }

  addPage(
    name: string,
    link: string,
    label: string,
    title?: string,
  ) {
    if (this.getAllPages().some(p => p.name === name)) {
      throw new Error(`There is already "${name}" page`);
    }

    const page = new PageBuilder(name, link, label, this.defaultLanguage, title)

    this.pages.push(page)

    return page
  }

  getPageByName(pageName: string) {
    const found =  this.getAllPages().find(p => p.name === pageName);

    if (!found) {
      throw new Error(`Page "${pageName}" not found`);
    }

    return found;
  }

  getAllLabels() {
    return [
      ...this.labels,
      ...this.reports.map(r => r.entity.getLabels()),
      ...this.getSavableEntities().map(e => e.entity.labels),
    ].flat();
  }

  assureLabelExists(label: string) {
    if (!this.getAllLabels().some(l => l === label)) {
      throw new Error(`There is no "${label}" page`);
    }
  }

  addGroupMenuItem(label: string) {
    const menuItem = new GroupMenuItemBuilder(this, label, this.defaultLanguage, 1)

    this.menuItems.push(menuItem)

    return menuItem
  }

  addInternalMenuItem(pageName: string) {
    const menuItem = new InternalMenuItemBuilder(this, pageName, this.defaultLanguage, 1)

    this.menuItems.push(menuItem)

    return menuItem
  }

  addExternalMenuItem(
    label: string,
    url: string,
  ) {
    const menuItem = new ExternalMenuItemBuilder(this, label, url, this.defaultLanguage, 1)

    this.menuItems.push(menuItem)

    return menuItem
  }

  addExternalEnvMenuItem(
    label: string,
    env: string,
  ) {
    const menuItem = new ExternalEnvMenuItemBuilder(this, label, env, this.defaultLanguage, 1)

    this.menuItems.push(menuItem)

    return menuItem
  }

  getBack(): DeploymentBuilder {
    return this.back
  }

  // documents
  getDocuments(): EntityBuilderWithOptions<DocumentBuilder>[] {
    return this.documents
  }

  addDocument(
    name: string,
    title?: {
      singular?: string,
      plural?: string,
    },
    options = this ? this.defOpts : ({} as any)
  ) {
    this.assureNoNamedEntityAlreadyDefined(name)

    const document = new DocumentBuilder(name, this.defaultLanguage, title)

    this.documents.push({ entity: document, options })

    return document
  }

  addReport(
    name: string,
    title?: string,
    options = this ? this.defOpts : ({} as any)
  ) {
    if (this.reports.some((f) => f.entity.name === name)) {
      throw new Error(
        `There is already report with name "${name}". Entity ${this.name}`
      )
    }

    const report = new ReportBuilder(name, this.defaultLanguage, title)

    this.reports.push({ entity: report, options })

    return report
  }

  assureNoNamedEntityAlreadyDefined(name: string) {
    if (
      this.getSavableEntities().some((f) => f.entity.name === name)
    ) {
      throw new Error(
        `There is already entity with name "${name}". Entity ${this.name}`
      )
    }
  }

  addInfoRegistry(
    name: string,
    registrarDepended: boolean,
    title?: {
      singular?: string,
      plural?: string,
    },
    options = this ? this.defOpts : ({} as any)
  ) {
    this.assureNoNamedEntityAlreadyDefined(name)

    const infoRegistry = new InfoRegistryBuilder(
      name,
      registrarDepended,
      this.defaultLanguage,
      title
    )

    this.infoRegistries.push({ entity: infoRegistry, options })

    return infoRegistry
  }

  addSumRegistry(
    name: string,
    registrarDepended: boolean,
    title?: {
      singular?: string,
      plural?: string,
    },
    options?: RegistryOptions,
  ) {
    this.assureNoNamedEntityAlreadyDefined(name)

    const sumRegistry = new SumRegistryBuilder(
      name,
      registrarDepended,
      this.defaultLanguage,
      title,
      options,
    )

    this.sumRegistries.push({ entity: sumRegistry, options: this.defOpts })

    return sumRegistry
  }

  addGlossaryTerm(term: string, definition: string) {
    if (this.glossary.some((g) => g.term === term)) {
      throw new Error(`There is already term "${term}" in glossary`)
    }

    this.glossary.push({ term, definition })

    return this
  }

  addCommnad(
    projectCategory: ProjectCategory,
    name: string,
    command: string,
    needFor: string
  ) {
    if (this.commands.some((g) => g.name === name)) {
      throw new Error(`There is already "${name}" command`)
    }

    this.commands.push({
      projectCategory,
      name,
      command,
      needFor,
    })

    return this
  }
  
  getSavableEntities() {
    return [
      this.catalogs,
      this.documents,
      this.infoRegistries,
      this.sumRegistries,
    ].flat();
  }

  getExternalSearchEntities(): Array<CatalogBuilder | DocumentBuilder | InfoRegistryBuilder | SumRegistryBuilder> {
    return this.getSavableEntities()
      .map(e => e.entity)
      .filter((e) => e.externalSearch)
  }

  addMethod(
    name: string,
    methodType: MethodType,
    title?: string,
  ): MethodBuilder {
    if (this.methods.some((f) => f.name === name)) {
      throw new Error(`There is already field with name "${name}" in args model`)
    }

    const field = new MethodBuilder(this, name, methodType, this.defaultLanguage, title)
    this.methods.push(field)

    return field
  }

  addLabel(label: string) {
    if (this.labels.some(m => m === label)) {
      throw new Error(`There is already system label "${label}"`);
    }

    this.labels.push(label);
  }

  getMethods() {
    return this.methods;
  }

  getModels() {
    return this.models;
  }

  // setMemory(request: string, limit?: string) {
  //   this.requests.memory = request

  //   if (limit) {
  //     this.limits.memory = limit
  //   } else {
  //     this.limits.memory = request
  //   }

  //   return this
  // }

  // setCpu(request: string, limit?: string) {
  //   this.requests.cpu = request

  //   if (limit) {
  //     this.limits.cpu = limit
  //   } else {
  //     this.limits.cpu = request
  //   }

  //   return this
  // }

  private _build(): System {
    const sortByName = <T extends { entity: { name: string } }>(entries: T[]) =>
      R.sortBy(
        R.compose(R.prop('name') as any, R.prop('entity')),
        entries
      ) as T[]

    return {
      name: this.name,
      prefix: this.prefix,
      needFor: this.needFor,
      deployEnvironments: this.deployEnvironments,
      glossary: R.sortBy(R.prop('term'), this.glossary),
      commands: R.sortBy(R.prop('command'), this.commands),
      telegramBots: R.sortBy(R.prop('name'), this.telegramBots).map((el) =>
        el.build()
      ),
      configVars: R.sortBy(R.prop('name'), this.configVars.map((cv) => cv.build())),
      catalogs: sortByName(this.catalogs).map((el) => el.entity.build()),
      documents: sortByName(this.documents).map((el) => el.entity.build()),
      infoRegistries: sortByName(this.infoRegistries).map((el) =>
        el.entity.build()
      ),
      sumRegistries: sortByName(this.sumRegistries).map((el) =>
        el.entity.build()
      ),
      languages: this.languages.sort(),
      defaultLanguage: this.defaultLanguage,
      reports: sortByName(this.reports).map(({ entity }) => entity.build()),
      restApis: R.sortBy(R.prop('name'), this.restApis).map(api =>
        api.build()
      ),
      integrationClients: R.sortBy(R.prop('name'), this.integrationClients).map(c =>
        c.build()
      ),
      workers: R.sortBy(R.prop('name'), this.workers).map((worker) =>
        worker.build()
      ),
      roles: R.sortBy(R.prop('name'), this.roles).map((role) => role.build()),
      menuItems: this.menuItems.map((item) => item.build()),
      pages: this.pages.map(p => p.build()),
      back: this.back.build(),
      models: this.models.map(m => m.build()),
      methods: this.methods.map(m => m.build()),
      labels: this.labels,
      additionalServices: this.additionalServices.map(p => p.build()),
    }
  }

  validate() {

    return this._build();
  }

  build(): System {
    this.validate();

    return this._build();
  }

  initDefaultCatalogs(): EntityBuilderWithOptions<CatalogBuilder>[] {
    addFilesCatalog(this)

    return this.catalogs
  }
}

export default SystemMetaBuilder
