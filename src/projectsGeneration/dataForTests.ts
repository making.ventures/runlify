import {BaseField} from './builders';

export const baseField: BaseField = {
    name: 'userId',
    title: { ru: '' },
    needFor: '',
    updatable: true,
    required: false,
    requiredOnInput: true,
    updatableByUser: true,
    showInList: true,
    showInCreate: true,
    showInEdit: true,
    showInFilter: true,
    showInShow: true,
    defaultDbValue: undefined,
    sharded: false,
};
