import { expect } from 'jest-without-globals'
import IdFieldBuilder from '../../../../builders/fields/IdFieldBuilder'
import ScalarFieldBuilder from '../../../../builders/fields/ScalarFieldBuilder'
import { genPrismaDefault } from './genPrismaDefault'

// yarn test --testPathPattern genPrismaDefault

describe('genPrismaDefault', () => {
  test('int not autoGenerated', () => {
    const someField = new IdFieldBuilder('someField', 'ru')
      .setType('bigint')
      .setAutoGenerated(false)
      .build()
    expect(genPrismaDefault(someField)).toBe('')
  })

  test('string not autoGenerated', () => {
    const someField = new IdFieldBuilder('someField', 'ru')
      .setType('string')
      .setAutoGenerated(false)
      .build()
    expect(genPrismaDefault(someField)).toBe('')
  })

  test('int autoGenerated', () => {
    const someField = new IdFieldBuilder('someField', 'ru')
      .setType('bigint')
      .setAutoGenerated(true)
      .build()
    expect(genPrismaDefault(someField)).toBe('@default(autoincrement())')
  })

  test('string autoGenerated', () => {
    const someField = new IdFieldBuilder('someField', 'ru')
      .setType('string')
      .setAutoGenerated(true)
      .build()
    expect(genPrismaDefault(someField)).toBe('@default(cuid())')
  })

  test('bool with true default value', () => {
    const someField = new ScalarFieldBuilder('someField', 'ru')
      .setType('bool')
      .setDefaultDbValue(true)
      .build()
    expect(genPrismaDefault(someField)).toBe('@default(true)')
  })

  test('int without changing autoGeneration', () => {
    const someField = new IdFieldBuilder('someField', 'ru')
      .setType('bigint')
      .build()
    expect(genPrismaDefault(someField)).toBe('@default(autoincrement())')
  })

  test('string without changing autoGeneration', () => {
    const someField = new IdFieldBuilder('someField', 'ru')
      .setType('string')
      .build()
    expect(genPrismaDefault(someField)).toBe('')
  })
})
