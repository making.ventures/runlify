import {
  GraphQLInt,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLSchema,
  GraphQLOutputType,
} from 'graphql'
import {pascalPlural, pascalSingular} from '../../../utils/cases'
import {genGraphFilterType} from './genGraphFilterType'
import {genGraphType} from './genGraphType'
import {genGraphIdField} from './fields/genGraphIdField'
import {genGraphField} from './fields/genGraphField'
import {Entity} from '../../builders/buildedTypes'
import {GraphQLVoid} from 'graphql-scalars'
import {getKeyField} from '../../metaUtils'

export const genGraphCrudSchema = (entity: Entity) => {
  const entitySchema = genGraphType(entity)
  const entityGraphFilterType = genGraphFilterType(entity)

  const listMetadataScheme = new GraphQLObjectType({
    fields: {
      count: { type: GraphQLInt },
    },
    name: 'ListMetadata',
  })

  const queryType = new GraphQLObjectType({
    name: 'Query',
    fields: {
      [pascalSingular(entity.name)]: {
        type: entitySchema,
        args: {
          ...genGraphIdField(getKeyField(entity)),
        },
      },
      [`all${pascalPlural(entity.name)}`]: {
        type: new GraphQLList(entitySchema),
        args: {
          page: { type: GraphQLInt },
          perPage: { type: GraphQLInt },
          sortField: { type: GraphQLString },
          sortOrder: { type: GraphQLString },
          filter: { type: entityGraphFilterType },
        },
      },
      [`_all${pascalPlural(entity.name)}Meta`]: {
        type: listMetadataScheme,
        args: {
          page: { type: GraphQLInt },
          perPage: { type: GraphQLInt },
          filter: { type: entityGraphFilterType },
        },
      },
    },
  })

  const mutationConfig = {
    name: 'Mutation',
    fields: {
      [`create${pascalSingular(entity.name)}`]: {
        type: entitySchema as GraphQLOutputType,
        args: getKeyField(entity).autoGenerated
          ? entity.fields
              .filter((f) => f.category !== 'id' && !f.hidden)
              // .filter(f => f.updatableByUser && f.category !== 'id' && !f.hidden)
              .reduce(
                (acc, cur) => ({ ...acc, ...genGraphField(cur, 'mutation') }),
                {}
              )
          : entity.fields
              .filter((f) => !f.hidden)
              // .filter(f => f.updatableByUser && !f.hidden)
              .reduce(
                (acc, cur) => ({ ...acc, ...genGraphField(cur, 'mutation') }),
                {}
              ),
      },
      [`update${pascalSingular(entity.name)}`]: {
        type: entitySchema as GraphQLOutputType,
        args: entity.fields
          .filter((f) => !f.hidden)
          // .filter(f => f.updatableByUser && !f.hidden)
          .reduce(
            (acc, cur) => ({ ...acc, ...genGraphField(cur, 'mutation') }),
            {}
          ),
      },
      [`remove${pascalSingular(entity.name)}`]: {
        type: entitySchema as GraphQLOutputType,
        args: genGraphIdField(getKeyField(entity)),
      },
    },
  }

  if (entity.type === 'document') {
    mutationConfig.fields = {
      ...mutationConfig.fields,
      [`rePost${pascalSingular(entity.name)}`]: {
        type: GraphQLVoid,
        args: genGraphIdField(getKeyField(entity)),
      },
    }
  }

  const mutationType = new GraphQLObjectType(mutationConfig)

  const schema = new GraphQLSchema({
    query: queryType,
    mutation: mutationType,
    types: [entitySchema, entityGraphFilterType, listMetadataScheme],
  })

  return schema
}
