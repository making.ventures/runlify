export type ShowComponentName =
  | 'DateField'
  | 'TextField'
  | 'NumberField'
  | 'BooleanField'
  | 'ReactMarkdownField'

export type LinkShowComponentName = 'ReferenceField'

export type LinkEditComponentName =
  | 'ReferenceInput'
  | 'SelectInput'
  | 'AutocompleteInput'
  | 'ReferenceArrayInput'
  | 'AutocompleteArrayInput'

export type EditComponentName =
  | 'TextInput'
  | 'NumberInput'
  | 'DateTimeInput'
  | 'DateInput'
  | 'BooleanInput'
