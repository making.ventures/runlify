# runlify CLI

A CLI for runlify.

## Customizing your CLI

Check out the documentation at https://github.com/infinitered/gluegun/tree/master/docs.

## Publishing to NPM

To package your CLI up for NPM, do this:

```shell
npm login
npm whoami
yarn test

yarn run build

npm publish
```

# License

MIT - see LICENSE

